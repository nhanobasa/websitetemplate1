$(async () => {
  var dfView // defaultViews
  // Preloader
  await $(window).on("load", function () {
    dfView = $(document).width()
    if ($("#preloader").length) {
      $("#preloader")
        .delay(800)
        .fadeOut("slow", function () {
          $(this).remove()
        })
    }
  })

  // Initiate the wowjs animation library
  new WOW().init()

  // Initiate superfish (Hỗ trợ cho dropdown menu)
  await $(".dropdown a").hover(function () {
    dfView = $(document).width()
    if (dfView > 780) {
      $(".navbar-nav").superfish({
        animation: {
          delay: 1000,
          opacity: "show",
          height: "show",
          autoArrows: false,
        },
        animationOut: {
          opacity: "hide",
        },
        speed: 600,
      })
    }
  })

  // Carosel for intro section
  $(".carousel").carousel({
    interval: 3000,
  })

  // back to top
  $(window).scroll(function (e) {
    if ($(this).scrollTop() > 100) {
      $(".back-to-top").fadeIn("slow")
    } else {
      $(".back-to-top").fadeOut("slow")
    }
  })
  $(".back-to-top").click(function () {
    $("html, body").animate(
      {
        scrollTop: 0,
      },
      1500,
      "easeInOutExpo"
    )
    return false
  })

  // Header scroll class (Tạo hiệu ứng khi scroll thì thanh navbar thay đổi)
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $("#header").addClass("header-scrolled")
    } else {
      $("#header").removeClass("header-scrolled")
    }
  })
  if ($(window).scrollTop() > 100) {
    $("#header").addClass("header-scrolled")
  }

  //Smooth scroll when click anchor link
  $(".nav-item a, .scrollto").on("click", function (e) {
    let target = $(this.hash)
    if (target.length) {
      $("html,body").animate(
        {
          scrollTop: $(target).offset().top - 72,
        },
        1500,
        "easeInOutExpo"
      )
    }
  })

  // jQuery counterUp (used in Facts section)
  $('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000,
  })
})
