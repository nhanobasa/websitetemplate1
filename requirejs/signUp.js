$(document).ready(function () {
  $("#signUp-form").validate({
    rules: {
      name: "required",
      username: {
        required: true,
        minlength: 2,
      },
      password: {
        required: true,
        minlength: 5,
      },
      confirm_password: {
        required: true,
        minlength: 5,
        equalTo: "#password",
      },
      email: {
        required: true,
        email: true,
      },
      agree: "required",
    },
    messages: {
      name: "Hãy điền tên của bạn!",
      username: {
        required: "Hãy điền username!",
        minlength: "Username của bạn phải có ít nhất 2 ký tự!",
      },
      password: {
        required: "Hãy điền password password!",
        minlength: "Mật khẩu không ngắn hơn 5 ký tự!",
      },
      confirm_password: {
        required: "Hãy điền confirm_password!",
        minlength: "Mật khẩu không ngắn hơn 5 ký tự!",
        equalTo: "Mật khẩu khôgn trùng khớp!",
      },
      email: "Hãy điền Email!",

      agree: "Bạn đồng ý?",
    },
  });
});
