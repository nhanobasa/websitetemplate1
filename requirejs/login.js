$(document).ready(function () {
  $("#login-form").validate({
    rules: {
      username: {
        required: true,
        minlength: 2,
      },
      password: {
        required: true,
        minlength: 5,
      },
    },
    messages: {
      username: {
        required: "Hãy điền username!",
        minlength: "Username của bạn phải có ít nhất 2 ký tự!",
      },
      password: {
        required: "Hãy điền password password!",
        minlength: "Mật khẩu khồng ngắn hơn 5 ký tự!",
      },
    },
  });
});
